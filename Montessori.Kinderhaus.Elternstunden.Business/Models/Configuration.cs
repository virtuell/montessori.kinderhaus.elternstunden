﻿using MailKit.Security;

namespace Montessori.Kinderhaus.Elternstunden.Business.Models
{
    /// <summary>
    /// Configuration items
    /// </summary>
    public class Configuration
    {
        /// <summary>
        /// SMTP server host
        /// </summary>
        public string SmtpServerHost { get; set; }

        /// <summary>
        /// SMTP server port
        /// </summary>
        public int SmtpServerPort { get; set; }

        /// <summary>
        /// SMTP client user name
        /// </summary>
        public string SmtpClientUsername { get; set; }

        /// <summary>
        /// SMTP client crypted password
        /// </summary>
        public string SmtpClientCryptedPassword { get; set; }

        /// <summary>
        /// Mail from-address
        /// </summary>
		public string EMailFrom { get; set; }

        /// <summary>
        /// Mail bcc-address
        /// </summary>
		public string EMailBcc { get; set; }

        /// <summary>
        /// Mail subject
        /// </summary>
		public string EMailSubject { get; set; }

        /// <summary>
        /// Mail content header
        /// </summary>
		public string EMailHeader { get; set; }

        /// <summary>
        /// Mail content footer
        /// </summary>
		public string EMailFooter { get; set; }

        /// <summary>
        /// Mail content footer picture
        /// </summary>
        public string EMailPicture { get; set; }

        /// <summary>
        /// Secret key for en-/decryption
        /// </summary>
        public string CrypoSecretKey { get; set; }

        /// <summary>
        /// True, if client should make an SSL-wrapped connection to the server, otherwise false
        /// </summary>
        public bool SmtpUseSsl { get; set; }

        /// <summary>
        /// Number of max. items to send in one smtpclient instance
        /// </summary>
        public decimal SmtpSendblock { get; set; }

        /// <summary>
        /// Initialize a new instance
        /// </summary>
        public Configuration()
        {
        }
    }
}

﻿namespace Montessori.Kinderhaus.Elternstunden.Business.Models
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Mail;

    /// <summary>
    /// Elternstunden Report
    /// </summary>
    public class Report
    {
        /// <summary>
        /// Family name
        /// </summary>
		public string Family { get; set; }

        /// <summary>
        /// Mail Address
        /// </summary>
		public List<string> MailAddresses { get; set; }

        public string MailAddress
        {
            get
            {
                if (MailAddresses == null) return "";
                return string.Join(";\n", MailAddresses.ToArray());
            }
        }

        /// <summary>
        /// True, if property MailAddress is a valid MailAddress
        /// </summary>
        public bool AreValidMailAddresses { get; set; }

        /// <summary>
        ///  Reporting Dictionary
        /// </summary>
		public Dictionary<string, string> ReportingDictionary { get; private set; }

        /// <summary>
        /// Get Reporting Overview
        /// </summary>
        public string ReportOverview
        {
            get
            {
                // set overview to empty string
                var overview = "";

                // iterate first four reporting dictionary items
                foreach (var item in ReportingDictionary.Take(4))
                {
                    // add ditionary key and value to overview
                    overview += item.Key + ":\t " + item.Value + "\t ";
                }

                // return reporting overview
                return overview;
            }

            set
            {
                // do nothing...
            }
        }

        /// <summary>
        /// Initialize a new instance
        /// </summary>
		public Report ()
		{
            // create a new reporting dictionary
			ReportingDictionary = new Dictionary<string, string> ();
		}
	}
}
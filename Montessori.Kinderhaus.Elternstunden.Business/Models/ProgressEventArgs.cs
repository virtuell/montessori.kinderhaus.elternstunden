﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Montessori.Kinderhaus.Elternstunden.Business.Models
{
    public class ProgressEventArgs : EventArgs
    {
        public string Message { get; set; }
    }
}

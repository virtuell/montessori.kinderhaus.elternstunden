﻿namespace Montessori.Kinderhaus.Elternstunden.Business.Controllers
{
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;
    
    public class ConfigurationsController
    {
        private string filePath;

        public ConfigurationsController(string filePath)
        {
            this.filePath = filePath;
        }

        public void WriteConfiguration(Models.Configuration configuration)
        {
			var serializer = new XmlSerializer(typeof(Business.Models.Configuration));

			var xml = "";

			using (var sWriter = new StringWriter())
			{
				XmlWriterSettings settigns = new XmlWriterSettings()
				{
					Indent = true,
				};
				using (XmlWriter writer = XmlWriter.Create(sWriter, settigns))
				{
					serializer.Serialize(writer, configuration);
					xml = sWriter.ToString();
                    File.WriteAllText(this.filePath, xml);
				}
			}
        }

        public Models.Configuration GetConfiguration()
        {
			Business.Models.Configuration configuration;

            XmlSerializer serializer = new XmlSerializer(typeof(Business.Models.Configuration));

            using (var reader = new StreamReader(this.filePath))
			{
				using (XmlTextReader xmlTextReader = new XmlTextReader(reader))
				{
					configuration = (Business.Models.Configuration)serializer.Deserialize(xmlTextReader);

				}
			}

            return configuration;
        }
    }
}

﻿namespace Montessori.Kinderhaus.Elternstunden.Business.Controllers
{
    using MailKit.Net.Smtp;
    using MimeKit;
    using MimeKit.Utils;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public class MailController
    {
        public event EventHandler<Models.ProgressEventArgs> MailProgress;

        /// <summary>
        /// List of mime messages
        /// </summary>
        public List<MimeMessage> MimeMessages { get; private set; }

        private Models.Configuration configuration;

        /// <summary>
        /// Initialize a new instance
        /// </summary>
        /// <param name="configuration">configuration details</param>
        public MailController(Models.Configuration configuration)
        {
            // set configuration to member
            this.configuration = configuration;

            // create a list of mime messages
            MimeMessages = new List<MimeMessage>();
        }

        /// <summary>
        /// Generate mime messages by report list
        /// </summary>
        /// <param name="reports">List of reports</param>
        public void GenerateMimeMessages(IEnumerable<Models.Report> reports)
        {
            // clear list if mime messages
            MimeMessages.Clear();

            // iterate reports with valid mail addresses
            foreach (var report in reports.Where(x=> x.AreValidMailAddresses))
            {
                // add a mime message to list by creating it out of the report
                MimeMessages.Add(createMimeMessage(report));
            }
        }

        /// <summary>
        /// Send mails
        /// </summary>
        public List<string> SendMails()
        {
            // send mails by list of mime messages
            return sendMails(MimeMessages);
        }

        
        private MimeMessage createMimeMessage(Models.Report report)
        {
            // create a new mime message
            var message = new MimeMessage();

            // add the sender address
            message.From.Add(new MailboxAddress(configuration.EMailFrom));

            // add the receiver addresses
            report.MailAddresses.ForEach(x => message.To.Add(new MailboxAddress(new System.Net.Mail.MailAddress(x.Trim()).Address)));

            // if bcc isn't empty, add the bcc address
            if (configuration.EMailBcc != string.Empty)
                message.Bcc.Add(new MailboxAddress(configuration.EMailBcc));
            
            // set message subject
            message.Subject = configuration.EMailSubject;

            // create a message body builder
            var builder = new BodyBuilder();

            // set a mime image as null
            MimeKit.MimeEntity image = null;

            // if a path for a message picture is given, add it to message
            if (configuration.EMailPicture != "")
            {
                // add picture as linked ressource
                image = builder.LinkedResources.Add(configuration.EMailPicture);

                // create image content id
                image.ContentId = MimeUtils.GenerateMessageId();
            }

            // create text-only body
            builder.TextBody = createTextBody(report);

            // create html body
            builder.HtmlBody = createHtmlBody(report, image);

            // set message body to message body builder
            message.Body = builder.ToMessageBody();

            // return mime message
            return message;
        }

        private string createTextBody(Models.Report report)
        {
            // set email text header
            var head = configuration.EMailHeader;

            // get max string length of reporing key values
            var maxLength = report.ReportingDictionary.Select(x => x.Key.Trim().Length).Max();

            // set content to empty string
            var content = "";

            // iterate reporting dictionary
            foreach (var item in report.ReportingDictionary)
            {
                // add dictionary item to content and build a readable matrix
                content += (item.Key.Trim() + ":").PadRight(maxLength + 1, ' ') + " \t" + item.Value + Environment.NewLine;
            }

            // set email footer
            var foot = configuration.EMailFooter;

            // remove placeholder of picture
            foot = foot.Replace("###BILD###", "");

            // build text-only message
            var body = head + content + foot;

            // return mail body
            return body;
        }

        private string createHtmlBody(Models.Report report, MimeEntity image)
        {
            // set email html header
            var head = HttpUtility.HtmlEncode(configuration.EMailHeader);

            // add html br-tag for new lines
            head = head.Replace(Environment.NewLine, "<br />" + Environment.NewLine);

            // get max string length of reporing key values
            var maxLength = report.ReportingDictionary.Select(x => x.Key.Trim().Length).Max();

            // set content to empty string
            var content = "";

            // iterate reporting dictionary
            foreach (var item in report.ReportingDictionary)
            {
                // add dictionary item to content and build a readable matrix
                content += (item.Key.Trim() + ":").PadRight(maxLength + 1, ' ') + " \t" + item.Value + Environment.NewLine;
            }

            // html encode content
            content = HttpUtility.HtmlEncode(content);

            // replace spaces with html entity for none breaking spaces (workaround for readable html matrix)
            content = content.Replace(" ", "&nbsp;");


            // add html br-tag for new lines
            content = content.Replace(Environment.NewLine, "<br />" + Environment.NewLine);

            // set html font style for readable matrix, using a fixed width font-family
            content = string.Format("<p style=\"font-family:Courier New,Courier,Lucida Sans Typewriter,Lucida Typewriter,monospace\">{0}</p>", content);

            // html encode mail footer
            var foot = HttpUtility.HtmlEncode(configuration.EMailFooter);

            // add html br-tag for new lines
            foot = foot.Replace(Environment.NewLine, "<br />" + Environment.NewLine);

            // add a last br-tag
            foot += "<br />" + Environment.NewLine;

            // if footer image isn't null, replace the picture placeholder with img-tag and content-id
            if (image != null)
                foot = foot.Replace("###BILD###", string.Format("<img src=\"cid:{0}\">", image.ContentId));

            // build html body
            var body = head + content + foot;

            // return html body
            return body;
        }

        private List<string> sendMails(List<MimeMessage> mimeMessages)
        {
            var errorList = new List<string>();

            // group messages, to avoid errors from mailserver, when sending to much mails in one smtpclient instance
            var mimeMessagesOuterGroup = GroupElements(mimeMessages, (int)configuration.SmtpSendblock);

            int currentMailInProgress = 0;

            // iterate the outer group and send n-times max. n (configuration.SmtpSendblock) messages within one smtpclient instance 
            foreach (var mimeMessageGroup in mimeMessagesOuterGroup)
            {
                // create a new stmtp client
                using (var client = new SmtpClient())
                {
                    // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    // connect to smtp client
                    client.Connect(configuration.SmtpServerHost, configuration.SmtpServerPort, configuration.SmtpUseSsl);


                    // Note: since we don't have an OAuth2 token, disable
                    // the XOAUTH2 authentication mechanism.
                    client.AuthenticationMechanisms.Remove("XOAUTH2");

                    // Note: only needed if the SMTP server requires authentication
                    client.Authenticate(
                        configuration.SmtpClientUsername,
                        Business.Controllers.CryptoController.Decrypt(configuration.SmtpClientCryptedPassword)
                    );

                    // iterate mime messages in current messagegroup 
                    foreach (var message in mimeMessageGroup)
                    {
                        // inform about progress...
                        var mailProgress = MailProgress;
                        if (mailProgress != null) mailProgress.Invoke(this, new Models.ProgressEventArgs { Message = $"Nachricht {++currentMailInProgress}/{mimeMessages.Count} wird gesendet..." });

                        try
                        {
                            // send message
                            client.Send(message);
                        }
                        catch (Exception ex)
                        {
                            errorList.Add(ex.Message);
                        }
                    }

                    // disconnect smtp client
                    client.Disconnect(true);
                }

            }

            return errorList;
        }

        private IEnumerable<IEnumerable<T>> GroupElements<T>(List<T> fullList, int batchSize)
        {
            int total = 0;
            while (total < fullList.Count)
            {
                yield return fullList.Skip(total).Take(batchSize);
                total += batchSize;
            }
        }

    }
}
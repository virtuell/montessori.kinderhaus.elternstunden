﻿namespace Montessori.Kinderhaus.Elternstunden.Business.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Net.Mail;

    /// <summary>
    /// Abrechnungscontroller
    /// </summary>
    public class AbrechnungsController
	{
        /// <summary>
        /// Get or set file path
        /// </summary>
		public string FilePath { get; private set; }

        /// <summary>
        /// Get or set first data row index
        /// </summary>
		public int FirstDataRowIndex { get; set; }

        /// <summary>
        /// Get or set header data row index
        /// </summary>
		public int HeaderDataRowIndex { get; set; }

        /// <summary>
        /// Get or set family column index
        /// </summary>
		public int FamilyColumnIndex { get; set; }

        /// <summary>
        /// Get or set mail address column index
        /// </summary>
		public int MailAdressColumnIndex { get; set; }

        /// <summary>
        /// Get or set first dictionary column index
        /// </summary>
		public int FirstDictionaryColumnIndex { get; set; }

        
        private SpreadsheetLight.SLDocument document;


        /// <summary>
        /// Initialize a new instance
        /// </summary>
        /// <param name="filePath">filepath of spreadsheet</param>
		public AbrechnungsController (string filePath)
		{
            // set variable to property
            FilePath = filePath;

            // create a new spreadsheet
            document = new SpreadsheetLight.SLDocument(FilePath);
		}

        /// <summary>
        /// Get spreadsheet worksheet names
        /// </summary>
        /// <returns>List of worksheet names</returns>
		public List<string> GetWorksheetNames ()
		{
            // get worksheet names
            return document.GetWorksheetNames ();
		}

        /// <summary>
        /// Select a worksheet
        /// </summary>
        /// <param name="worksheetName">Name of worksheet</param>
        /// <returns>True, if worksheet is selected</returns>
		public bool SelectWorksheet (string worksheetName)
		{
            // select worksheet
            return document.SelectWorksheet (worksheetName);
		}

        /// <summary>
        /// Get reports
        /// </summary>
        /// <returns>List of reports</returns>
        public List<Models.Report> GetReports()
        {
            // create a new report list
            var reports = new List<Models.Report>();

            // get spreadsheet statistics 
            var statistics = document.GetWorksheetStatistics();

            // set last used column index
            var lastColumnIndex = setLastColumnIndex(document, statistics);

            // iterate spreadsheet rows
            for (int rowIndex = FirstDataRowIndex; rowIndex <= statistics.EndRowIndex; rowIndex++)
            {
                // prepare mail address validation
                var areValidMailAddresses = false;
                List<string> mailAddressList = null;
                try
                {
                    // get mail address from document
                    mailAddressList = getSplittedMailAddresses(document.GetCellValueAsString(rowIndex, MailAdressColumnIndex));
                    if (mailAddressList.Count == 0)
                        throw new Exception("No valid mail address");
                    // try to create a mailaddresses
                    mailAddressList.ForEach(x => new MailAddress(x));

                    // no validation error, so set validation flag to true
                    areValidMailAddresses = true;
                }
                catch (System.Exception ex)
                {
                    // no valid mail address. Do nothing, because validation flag was
                    // initialized with false
                }

                // create a new report
                var report = new Models.Report
                {
                    Family = document.GetCellValueAsString(rowIndex, FamilyColumnIndex),
                    MailAddresses = mailAddressList,
                    AreValidMailAddresses = areValidMailAddresses,
                };

                // if report properties are valid, add report to list
                if (AreReportPropertiesValid(report))
                    reports.Add(report);
                else
                    continue;

                // fill reporting details by document
                FillReportingDictionary(document, lastColumnIndex, rowIndex, report);
            }

            // return generated reports
            return reports;
        }
        
        private List<string> getSplittedMailAddresses(string emailAddressesWithDelimiter)
        {
            var untrimmedAddresses = new List<string>(emailAddressesWithDelimiter.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries));
            var trimmedAddresses = new List<string>();
            untrimmedAddresses.ForEach(x => trimmedAddresses.Add(x.Trim()));

            return trimmedAddresses;
        }

        public bool AreReportPropertiesValid (Models.Report report)
		{
            // family or mail address has to be set as minmum criteria 
			var reportPropertiesAreValid = report.Family.Trim () != string.Empty ||
			                               report.MailAddresses.Count > 0;

            // true, if report properties are valid
			return reportPropertiesAreValid;
		}

        /// <summary>
        /// Get current worksheet name
        /// </summary>
        /// <returns>name of workshhet</returns>
        public string GetCurrentWorksheetName()
        {
            // return current worksheet name
            return document.GetCurrentWorksheetName();
        }



		private void FillReportingDictionary (SpreadsheetLight.SLDocument document, int lastColumnIndex, int rowIndex, Models.Report report)
		{
            // loop spreadsheet columns of a report row
            for (int columnIndex = FirstDictionaryColumnIndex; columnIndex <= lastColumnIndex; columnIndex++)
			{
                // set column header
				var headerValue = document.GetCellValueAsString (HeaderDataRowIndex, columnIndex);

                // set detail value
                var detailValue = document.GetCellValueAsString(rowIndex, columnIndex);

                // continue to next column, if no detail value is given
                if (detailValue.Trim() == string.Empty)
					continue;

                //check, if dictionary contains the given header value
                if(report.ReportingDictionary.ContainsKey(headerValue))
                {
                    // init new header value
                    var headerValueNew = "";

                    // init header value counter
                    var counter = 0;

                    // build nuew header value, while the computed value exists in dictionary
                    do
                    {
                        // set new header value
                        headerValueNew = string.Format("{0} ({1})", headerValue, ++counter);
                    }
                    while (report.ReportingDictionary.ContainsKey(headerValueNew));

                    // set new header value to current header value
                    headerValue = headerValueNew;
                }

                try
                {
                    // add a new dictionary item
                    report.ReportingDictionary.Add(headerValue, detailValue.Trim());

                }
                catch (System.Exception)
                {
                    // uhps, throw this unknown error...
                    throw;
                }
			}
		}
		
        private int setLastColumnIndex (SpreadsheetLight.SLDocument document, SpreadsheetLight.SLWorksheetStatistics statistics)
		{
            // init last column index
			var lastColumnIndex = 1;

            // loop reverse columns
			for (int columnIndex = statistics.EndColumnIndex; columnIndex > 0; columnIndex--)
			{
                // set last column value in the header data row
				var lastValue = document.GetCellValueAsString (HeaderDataRowIndex, columnIndex);

                // if last value isn't empty, update last column index
				if (lastValue.Trim () != string.Empty)
				{
                    // update last (used) column index and leave loop
					lastColumnIndex = columnIndex;
					break;
				}
			}

            // return last used column index of header data row
			return lastColumnIndex;
		}


    }

}


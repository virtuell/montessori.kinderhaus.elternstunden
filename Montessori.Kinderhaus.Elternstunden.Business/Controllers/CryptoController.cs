﻿namespace Montessori.Kinderhaus.Elternstunden.Business.Controllers
{
    using Montessori.Kinderhaus.Elternstunden.Business.Properties;
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;

    public static class CryptoController
    {
        /// <summary>
        /// Get or set secret key
        /// </summary>
        public static string SecretKey
        {
            set
            {
                // encode secret key string as byte array
                secretKey = ASCIIEncoding.ASCII.GetBytes(value);
            }

            get
            {
                // encode secret key byte array as string
                return ASCIIEncoding.ASCII.GetString(secretKey);
            }
        }

        private static byte[] secretKey;

        private static byte[] initialVector = ASCIIEncoding.ASCII.GetBytes(Resources.InitialVector);

        public static string Encrypt(string originalString)
		{
            // if original string is empty, throw an exception
			if (String.IsNullOrEmpty(originalString))
				throw new ArgumentNullException ("The string which needs to be encrypted can not be null.");

            // create a DES crypto service provider
			DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();

            // create a new memory stream
            MemoryStream memoryStream = new MemoryStream();

            // create a new crypto stream using the memory stream
            CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateEncryptor(secretKey, initialVector), CryptoStreamMode.Write);

            // create a new stream writer out of crypto stream
            StreamWriter writer = new StreamWriter(cryptoStream);

            // write and flush the crypto streamwriter
            writer.Write(originalString);
			writer.Flush();

            // flush and delete buffer
            cryptoStream.FlushFinalBlock();

            // flush crypto streamwriter
			writer.Flush();

            // return the encrypted base64 string
            return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);

		}

		public static string Decrypt(string cryptedString)
		{
            // if crypted string is empty, throw an exception
			if (String.IsNullOrEmpty(cryptedString))
				throw new ArgumentNullException ("The string which needs to be decrypted can not be null.");

            // create a DES crypto service provider
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();

            // create a memory stream by base64 crypted string
            MemoryStream memoryStream = new MemoryStream (Convert.FromBase64String(cryptedString));

            // create a new crypto stream using the secret key 
            CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateDecryptor(secretKey, initialVector), CryptoStreamMode.Read);

            // create a new stream reader with crypto stream
            StreamReader reader = new StreamReader(cryptoStream);

            // return the decrypted string
            return reader.ReadToEnd();
		}
    }
}

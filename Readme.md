# Elternstunden E-Mail

Die Anwendung versendet den aktuellen Stand der Elternstunden per E-Mail an die jeweils eingetragenen Familien

## Anwendung installieren

Die aktuelle Version der Anwendung befindet sich unter: https://bitbucket.org/virtuell/montessori.kinderhaus.elternstunden/downloads/

## Dokumentation

Die Anwendungsdokumentation befindet sich unter: https://bitbucket.org/virtuell/montessori.kinderhaus.elternstunden/wiki/

## Kanban-Board / Issues

Bitbucket Mitglieder k�nnen Issues verfolgen: https://bitbucket.org/virtuell/montessori.kinderhaus.elternstunden/addon/com.comalatech.bitbucket.canvas/canvas-repo-page 

Neue Issues k�nnen unter dieser Adresse auch ohne Anmeldung erstellt werden: https://bitbucket.org/virtuell/montessori.kinderhaus.elternstunden/issues/new

## Mitwirken

Vorhandene Issues oder neue Features k�nnen gerne gemeinsam implementiert werden. 

### Wie richte ich die Softwareentwicklungsplattfrom ein?

* Microsoft Visual Studio Community 2017 
* Zielframework: .NET Framework 4.5
* Projektabh�ngigkeiten (NuGet Pakete):
	* BouncyCastle
	* DocumentFormat.OpenXml
	* MailKit
	* MimeKit
	* SpreadsheetLight
	* NUnit 3 Test Adapter for Visual Studio
* Aktualisierungen m�ssen alle Tests in der Projektmappe **Montessori.Kinderhaus.Elternstunden.Business.Tests** bestehen
* Neue Versionen werden �ber das Setupprojekt **Montessori.Kinderhaus.Elternstunden.Setup** erstellt
  * Die Versionierung erfolgt nach einem datumsbasiertem Schema: **Jahr.Monat.Nebenversion.Buildnummer**: 17.6.0.*
  * Die Bereitstellung neuer MSI-Pakete erfolgt im Download Bereich dieses Repositories: https://bitbucket.org/virtuell/montessori.kinderhaus.elternstunden/downloads/
* Softwareversionen m�ssen im Repository mit einem Tag nach dem oben aufgef�hrten datumsbasiertem Schema (Jahr.Monat.Nebenversion) gekennzeichnet werden

### Ansprechpartner

Andr� Ehlert

Allgemeiner Kontakt kann �ber Erstellung eines neuen Vorgangs hergestellt werden. 

Bitte die Komponente **Contact** ausw�hlen: https://bitbucket.org/virtuell/montessori.kinderhaus.elternstunden/issues/new

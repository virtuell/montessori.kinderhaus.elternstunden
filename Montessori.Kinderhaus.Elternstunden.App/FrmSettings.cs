﻿namespace Montessori.Kinderhaus.Elternstunden.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Montessori.Kinderhaus.Elternstunden.App.Properties;

    public partial class FrmSettings : Form
    {
        private string cryptedPassword;

        public FrmSettings()
        {
            // init components
            InitializeComponent();
        }

        private void FrmSettings_Load(object sender, EventArgs e)
        {
            try
            {
                // set crypted password
                cryptedPassword = Settings.Default.SmtpClientPassword;

                // set icon for error provider
                errorProviderIntegerHigherZero.Icon = Resources.StatusCriticalError;
            }
            catch (Exception ex)
            {
                // show exception message
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Settings.Default.Reload();
                DialogResult = System.Windows.Forms.DialogResult.Cancel;
                Close();
            }
            catch (Exception ex)
            {
                // show exception message
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                // if user has changed the password, it has to be encrypted
                if (cryptedPassword != tbxCryptedPassword.Text)
                    tbxCryptedPassword.Text = Business.Controllers.CryptoController.Encrypt(tbxCryptedPassword.Text);

                // save application settings
                Settings.Default.Save();

                // set dialog result
                DialogResult = System.Windows.Forms.DialogResult.OK;

                // close window
                Close();
            }
            catch (Exception ex)
            {
                // show exception message
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void tbx_ValidatingIntegerHigherZero(object sender, CancelEventArgs e)
        {
            try
            {
                // cast sender into a new textbox variable 
                var tbx = sender as TextBox;

                // init integer for try-parse operation
                var parsedInteger = 0;

                // if textbox value can't parse as integer, show error and cancel validation
                if (!int.TryParse(tbx.Text, out parsedInteger))
                {
                    // cancel validation
                    e.Cancel = true;

                    // select all textbox text
                    tbx.SelectAll();

                    //show error message
                    errorProviderIntegerHigherZero.SetError(tbx, "Bitte geben Sie eine Ganzzahl > 0 ein!");
                }
            }
            catch (Exception ex)
            {
                // show exception message
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void tbx_ValidatedIntegerHigherZero(object sender, EventArgs e)
        {
            try
            {
                // cast sender into a new textbox variable
                var tbx = sender as TextBox;

                // remove error message
                errorProviderIntegerHigherZero.SetError(tbx, "");
            }
            catch (Exception ex)
            {
                // show exception message
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void btnSelectPicture_Click(object sender, EventArgs e)
        {
            try
            {
                // show open file dialog
                openFileDialogPicture.ShowDialog();
            }
            catch (Exception ex)
            {
                // show exception message
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void openFileDialogPicture_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                // set path to selected picture in application settings
                Settings.Default.SmtpMailPicture = openFileDialogPicture.FileName;
            }
            catch (Exception ex)
            {
                // show exception message
                MessageBox.Show(ex.Message,Text);
            }
        }
    }
}

﻿namespace Montessori.Kinderhaus.Elternstunden.App
{
    using Montessori.Kinderhaus.Elternstunden.App.Properties;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {
        private Business.Controllers.AbrechnungsController abrechnungsController;
        private Exception exceptionBgw;
        private List<string> sendMailErrorList;

        public Form1()
        {
            // init components
            InitializeComponent();

            // set form icon from application ressources
            this.Icon = Resources.TeamProjectRepositoryFolder;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                // set window size by user setting
                this.Size = Settings.Default.Form1Size;

                // set window state by user setting
                this.WindowState = Settings.Default.Form1State;

                // set window text by application setting
                this.Text = Settings.Default.Form1Text;

                // set secret key for crytographic controller
                Business.Controllers.CryptoController.SecretKey = Settings.Default.CryptoSecretKey;

                // hide progressbar & clear progresstext
                progressBar.Visible = false;
                progressText.Text = "";

            }
            catch (Exception ex)
            {
                // show exception details
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            try
            {
                // Migrate settings, if software version has changed
                MigrateUserSettingsAfterSoftwareUpdate();
            }
            catch (Exception ex)
            {
                // show error message
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                // if window state is normal, save windows size in application settings
                if(this.WindowState == FormWindowState.Normal)
                    Settings.Default.Form1Size = this.Size;

                // save window state in application settings
                Settings.Default.Form1State = this.WindowState == FormWindowState.Minimized ? FormWindowState.Normal : this.WindowState;

                // save application settings
                Settings.Default.Save();
            }
            catch (Exception ex)
            {
                // show exception details
                MessageBox.Show(ex.Message, Text);
            }
        }



        private void menuFileOpen_Click(object sender, EventArgs e)
        {
            try
            {
                // open a file dialog, to choose a xlsx file and set result of dialog 
                var result = openFileDialogSheet.ShowDialog();

                // display filename, if user has opened a file
                if(result == DialogResult.OK)
                {
                    // set file info, to display filename as additional window/formular text
                    var fileInfo = new FileInfo(openFileDialogSheet.FileName);
                    Text = Settings.Default.Form1Text + " - " + fileInfo.Name;
                }
            }
            catch (Exception ex)
            {
                // show exception details
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void menuSettings_Click(object sender, EventArgs e)
        {
            try
            {
                // create a new settings window
                using (var frm = new FrmSettings { Icon = this.Icon })
                {
                    // show settings window as dialog
                    var dialogResult = frm.ShowDialog();

                    // if settings are changed, the user should restart the application in case of an opened file
                    if(dialogResult == DialogResult.OK &&
                        !string.IsNullOrEmpty(openFileDialogSheet.FileName))
                    {
                        MessageBox.Show("Bitte starten Sie die Anwendung neu, um die geänderten Einstellungen zu laden!", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                // show exception details
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void menuHelpInfoAbout_Click(object sender, EventArgs e)
        {
            try
            {
                // create a new info about window and show as dialog
                using (var frm = new FrmAbout(typeof(Form1).Assembly) { Icon = this.Icon, })
                    frm.ShowDialog();
            }
            catch (Exception ex)
            {
                // show exception details
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void menuHelpDoku_Click(object sender, EventArgs e)
        {
            try
            {
                // show online documentation
                Process.Start(Settings.Default.WikiUrlRepository);
            }
            catch (Exception ex)
            {
                // show exception details
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void menuExit_Click(object sender, EventArgs e)
        {
            try
            {
                // exit application
                Application.Exit();
            }
            catch (Exception ex)
            {
                // show exception details
                MessageBox.Show(ex.Message, Text);
            }
        }



        private void openFileDialogSheet_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                // create new abrechnungs controller by file
                abrechnungsController = new Business.Controllers.AbrechnungsController(openFileDialogSheet.FileName)
                {
                    FamilyColumnIndex = int.Parse(Settings.Default.SheetFamilyColumnIndex),
                    FirstDataRowIndex = int.Parse(Settings.Default.SheetFirstDataRowIndex),
                    FirstDictionaryColumnIndex = int.Parse(Settings.Default.SheetFirstDictionaryColumnIndex),
                    HeaderDataRowIndex = int.Parse(Settings.Default.SheetHeaderDataRowIndex),
                    MailAdressColumnIndex = int.Parse(Settings.Default.SheetMailAddressColumnIndex),
                };

                // get current worksheet name, to set selected item later on in combo box 
                var currentWorksheetName = abrechnungsController.GetCurrentWorksheetName();

                // remove selected index changed event handler
                comboSelectSheet.SelectedIndexChanged -= comboSelectSheet_SelectedIndexChanged;

                // set combobox datasource with all worksheetnames of selected file
                comboSelectSheet.DataSource =
                    abrechnungsController.GetWorksheetNames();

                // add the removed selected index changed event handler
                comboSelectSheet.SelectedIndexChanged += comboSelectSheet_SelectedIndexChanged;

                // set the current worksheet name of selected file in combobox, so the selected index changed event will be raised
                comboSelectSheet.SelectedItem = currentWorksheetName;

            }
            catch (Exception ex)
            {
                // show exception details
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void comboSelectSheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // do nothing, if selected index is negativ (nothing is selected)
                if (comboSelectSheet.SelectedIndex < 0) return;

                // set active spreadsheet by value of selected combobox item
                var selected = abrechnungsController.SelectWorksheet(comboSelectSheet.SelectedItem.ToString());

                // throw an exception, if something went wrong while selecting the spreadsheet
                if (!selected)
                    throw new Exception(string.Format("Tabellenblatt \"{0}\" kann nicht geladen werden", comboSelectSheet.SelectedItem));

                // generate reports
                var reports = abrechnungsController.GetReports();

                // unset and set datasource with geneerated reoports of report binding source - this will be displayed in datagridview
                reportBindingSource.DataSource = null;
                reportBindingSource.DataSource = reports;
            }
            catch (Exception ex)
            {
                // show exception details
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void lnkSelectAll_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                // set all rows of datagridview as selected
                dgvOverview.Rows.Cast<DataGridViewRow>().All(x => x.Selected = true);
            }
            catch (Exception ex)
            {
                // show exception details
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void lnkInverseSelection_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                // iterate rows of datagridview
                foreach (DataGridViewRow row in dgvOverview.Rows)
                {
                    // inverse selected property
                    row.Selected = !row.Selected;
                }                   
            }
            catch (Exception ex)
            {
                // show exception details
                MessageBox.Show(ex.Message, Text);
            }

        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                // get selected rows of datagridview
                var selectedRows = dgvOverview.Rows.Cast<DataGridViewRow>().Where(x => x.Selected);

                // get reports out of data bound item from selected rows in datagridview
                var reports = selectedRows.Select(x => (Business.Models.Report)x.DataBoundItem);

                // check, if selection contains invalid mail addresses
                var invalid = reports.Any(x => !x.AreValidMailAddresses);

                // if reports contains invalid mail addresses, inform that only reports with valid ones will be sent
                if(invalid)
                    MessageBox.Show("Es können nur Berichte mit gültigen E-Mail Addressen versendet werden.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);

                // if no valid report is selectd cancel the operation
                if(!reports.Any(x=> x.AreValidMailAddresses))
                {
                    // show info and return
                    MessageBox.Show("Sie haben keine Einträge mit gültigen Berichten ausgewählt", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                // ask, if really want to send [selcted row count] mails
                var countFamilies = reports.Where(x => x.AreValidMailAddresses).Count();
                string message = string.Format("Wollen Sie wirklich E-Mails an {0} Familie{1} senden?", countFamilies, countFamilies != 1 ? "n" : "");
                var result = MessageBox.Show(message, Text, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

                // cancel, if user don't want to send mails...
                if (result != System.Windows.Forms.DialogResult.Yes)
                    return;

                progressText.Text = "E-Mailversand wird gestartet...";
                progressBar.Visible = true;
                bgwWorker.RunWorkerAsync(reports);
              
            }
            catch (System.Security.Cryptography.CryptographicException ex)
            {
                // show crypto error
                MessageBox.Show(ex.Message + " Bitte geben Sie Ihr Kennwort (Einstellungen - E-Mail Konto) erneut ein!", Text);
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                // show socket exception
                MessageBox.Show(ex.Message, Text);
            }
            catch (Exception ex)
            {
                // show exception details
                MessageBox.Show(ex.ToString(), Text);
            }
        }

        private void dgvOverview_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                // only process with data containing rows
                if (e.RowIndex < 0) return;

                // set databound item of row
                var report = (Business.Models.Report)dgvOverview.Rows[e.RowIndex].DataBoundItem;

                // set style for mail address cell
                if(e.ColumnIndex == mailAddressDataGridViewTextBoxColumn.Index)
                {
                    // get bg-color of familiy cell
                    var familyBackgroundColor = dgvOverview.Rows[e.RowIndex]
                        .Cells[familyDataGridViewTextBoxColumn.Index].Style.BackColor;

                    // get current report mail cell
                    var mailCell = dgvOverview.Rows[e.RowIndex].Cells[e.ColumnIndex];

                    // if report contains a valid mail address, use default background color and remove tooltiptext,
                    // otherwise highlight cell and add tooltiptext
                    if(report.AreValidMailAddresses)
                    {
                        // set background color to default value
                        mailCell.Style.BackColor = familyBackgroundColor;

                        // remove tooltip text
                        mailCell.ToolTipText = "";
                    }
                    else
                    {
                        // change background color to highlight the cell
                        mailCell.Style.BackColor = Color.FromKnownColor(KnownColor.LightCoral);

                        // add tooltip error message
                        mailCell.ToolTipText = "Bitte tragen Sie eine gültige E-Mail Addresse ein.";
                        mailCell.ToolTipText += " Andernfalls kann der Bericht nicht versendet werden!";

                    }
                }
            }
            catch (Exception ex)
            {
                // show exception details
                MessageBox.Show(ex.Message, Text);
            }
        }



        private IEnumerable<PropertyInfo> GetUserSettings(ApplicationSettingsBase applicationSettingsBase)
        {
            /*
             * https://social.msdn.microsoft.com/Forums/en-US/565f9380-da42-4c69-8426-99578b9f7daf/how-to-iterate-user-scope-only-myapppropertiessettingsdefault?forum=csharpgeneral
             */

            // return only user scoped application settings
            return applicationSettingsBase.GetType().GetProperties()
                .Where(x => x.GetCustomAttributes(typeof(UserScopedSettingAttribute), false)?.Any() ?? false);
        }

        private void MigrateUserSettingsAfterSoftwareUpdate()
        {
            try
            {
                // init flag for settings dialog
                var showSettingsDialog = false;

                // if "updated user settings" flag is false, migrate user settings
                if (!Settings.Default.UpdatedUserSettings)
                {
                    // get previous product version setting
                    var previousVersion = Settings.Default.GetPreviousVersion("CurrentVersion");

                    /*
                     * if previous version was "17.6.0", the user MUST edit settings
                     * and retype the mail password, because of a crypto processing change.
                     * Indicator for the mentioned version is, that previeousVersion is empty
                     */
                    if(string.IsNullOrEmpty((string)previousVersion))
                    {
                        // message builder for warning output
                        StringBuilder messageBuilder = new StringBuilder();
                        messageBuilder.AppendLine("Bitte geben Sie das E-Mail Password erneut ein.");
                        messageBuilder.AppendLine();
                        messageBuilder.AppendLine("Andernfalls können keine E-Mails gesendet werden.");
                        messageBuilder.AppendLine();
                        messageBuilder.AppendLine("Hierbei handelt es sich um eine einmalige Änderung, da die Sicherheitseinstellungen erhöht wurden!");
                        
                        // show messagebox 
                        MessageBox.Show(messageBuilder.ToString(), Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        // set "show settings" flag to true
                        showSettingsDialog = true;
                    }

                    // get current user settings
                    var userSettings = GetUserSettings(Settings.Default);

                    // iterate current user settings
                    foreach (var item in userSettings)
                    {
                        // read previous value of user setting
                        var previousValue = Settings.Default.GetPreviousVersion(item.Name);

                        // if previous value isn't null and values doesn't equal, 
                        // update current value
                        if (previousValue != null && !Settings.Default[item.Name].Equals(previousValue))
                        {
                            // write previous value into current setting
                            Settings.Default[item.Name] = previousValue;
                        }
                    }

                    // set user setting current product version 
                    Settings.Default.CurrentVersion = Application.ProductVersion;

                    // set "updated user settings" flag
                    Settings.Default.UpdatedUserSettings = true;

                    // save changes
                    Settings.Default.Save();

                    // reload (changed) settings
                    Settings.Default.Reload();

                    // show settings dialog, if "show settings" flag is true
                    if(showSettingsDialog)
                    {
                        // create a new settings window and show as dialog
                        using (var frm = new FrmSettings { Icon = this.Icon })
                            frm.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                // show error message
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void bgwWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                // create a configuration object with all necessary application settings
                var configuration = new Business.Models.Configuration
                {
                    EMailBcc = Settings.Default.SmtpClientBcc,
                    EMailFooter = Settings.Default.SmtpMailFooter,
                    EMailFrom = Settings.Default.SmtpClientFrom,
                    EMailHeader = Settings.Default.SmtpClientMailHeader,
                    EMailSubject = Settings.Default.SmtpClientSubject,
                    EMailPicture = Settings.Default.SmtpMailPicture,
                    SmtpClientCryptedPassword = Settings.Default.SmtpClientPassword,
                    SmtpClientUsername = Settings.Default.SmtpClientUsername,
                    SmtpServerHost = Settings.Default.SmtpServerHost,
                    SmtpServerPort = int.Parse(Settings.Default.SmtpServerPort),
                    SmtpUseSsl = Settings.Default.SmtpServerUseSsl,
                    SmtpSendblock = Settings.Default.SmtpSendBlock,
                    CrypoSecretKey = Settings.Default.CryptoSecretKey,
                };


                // create a new mail controller with configuration settings
                var controller = new Business.Controllers.MailController(configuration);

                // add progress listener
                controller.MailProgress += Controller_MailProgress;

                try
                {
                    // generate mail messages out of bgw argument
                    var reports = e.Argument as IEnumerable<Business.Models.Report>;
                    controller.GenerateMimeMessages(reports);

                    // send mails
                    sendMailErrorList = controller.SendMails();
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    // remove progress listener
                    controller.MailProgress -= Controller_MailProgress;
                }
            }
            catch (Exception ex)
            {
                exceptionBgw = ex;
            }
        }

        private void Controller_MailProgress(object sender, Business.Models.ProgressEventArgs e)
        {
            try
            {
                bgwWorker.ReportProgress(1, e.Message);
            }
            catch (Exception)
            {
            }
        }

        private void bgwWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressText.Text = e.UserState.ToString();
        }

        private void bgwWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // hide progressbar
            progressBar.Visible = false;

            // show error or success message
            if(exceptionBgw != null)
            {
                progressText.Text = exceptionBgw.Message;
                MessageBox.Show(exceptionBgw.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                exceptionBgw = null;
            }
            else if (sendMailErrorList != null && sendMailErrorList.Count > 0)
            {
                progressText.Text = "Es konnten nicht alle Berichte gesendet werden!";
                string errors = string.Join(Environment.NewLine, sendMailErrorList.ToArray());
                MessageBox.Show(errors, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                progressText.Text = "Die Berichte wurden gesendet!";
                MessageBox.Show("Die Berichte wurden gesendet!", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
    }
}

﻿namespace Montessori.Kinderhaus.Elternstunden.App
{
    using Montessori.Kinderhaus.Elternstunden.App.Properties;
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Windows.Forms;

    public partial class FrmAbout : Form
    {
         private Assembly executingAssembly;

        /// <summary>
        /// Initialisiert eine neue Instanz der KATAG.About.Formular-Klasse.
        /// </summary>
        /// <param name="assembly">Gibt die Assembly an, deren Informationen angezeigt werden</param>
        /// <param name="icon">Legt das Symbol für das Formular fest</param>
        public FrmAbout(Assembly assembly = null, System.Drawing.Icon icon = null)
        {
            // init components
            InitializeComponent();

            // set executing assembly
            this.executingAssembly = assembly != null ? assembly : typeof(FrmAbout).Assembly;
            
            // if icon wasn't null, set window icon
            if (icon != null)
                Icon = icon;
        }

        private void FrmAbout_Load(object sender, EventArgs e)
        {
            try
            {
                // set window text 
                this.Text = "Info über " + executingAssembly.GetName().Name;

                // set text for product label
                lblProdukt.Text = string.Format("{0} {1}",
                    executingAssembly.GetName().Name,
                    executingAssembly.GetName().Version
                    );

                // get copyright information of assembly
                object[] oCopyright = executingAssembly.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);

                // get company information of assembly
                object[] oCompany = executingAssembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);

                // set copyright string
                string copyright = oCopyright.Length != 0 ? ((AssemblyCopyrightAttribute)oCopyright[0]).Copyright : "";

                // set company string
                string company = oCompany.Length != 0 ? ((AssemblyCompanyAttribute)oCompany[0]).Company : "";

                // set copyright range, if it starts in another year
                if (!copyright.EndsWith(DateTime.Now.Year.ToString()))
                    copyright += " - " + DateTime.Now.Year.ToString();

                // set copyright text for copyright label
                lblCopyright.Text = copyright;

                // set company text for company label
                lblFirma.Text = company;

                // iterate referenced assemblies
                foreach (var item in executingAssembly.GetReferencedAssemblies().OrderBy(x => x.Name))
                {
                    // add assembly information in list box
                    lbxKomponenten.Items.Add(string.Format("{0} {1}", item.Name, item.Version));
                }
            }
            catch (Exception ex)
            {
                // show exception message
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                // close window
                Close();

            }
            catch (Exception ex)
            {
                // show exception message
                MessageBox.Show(ex.Message, Text);
            }
        }

        private void lnkExternalComponents_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                // show license information for extneral components in online documentation
                Process.Start(Settings.Default.WikiUrlExternalComponents);
            }
            catch (Exception ex)
            {
                // show exception message
                MessageBox.Show(ex.Message, Text);
            }
        }
    }
}

﻿namespace Montessori.Kinderhaus.Elternstunden.App
{
    partial class FrmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSettings));
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.openFileDialogPicture = new System.Windows.Forms.OpenFileDialog();
            this.errorProviderIntegerHigherZero = new System.Windows.Forms.ErrorProvider(this.components);
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnSelectPicture = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.label17 = new System.Windows.Forms.Label();
            this.numVersandBlock = new System.Windows.Forms.NumericUpDown();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.tbxCryptedPassword = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tbxSmtpServer = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderIntegerHigherZero)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numVersandBlock)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(602, 519);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Abbrechen";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(494, 519);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 28);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // openFileDialogPicture
            // 
            this.openFileDialogPicture.Filter = "JPG-Dateien|*.jpg";
            this.openFileDialogPicture.Title = "Bild für E-Mail Footer auswählen...";
            this.openFileDialogPicture.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialogPicture_FileOk);
            // 
            // errorProviderIntegerHigherZero
            // 
            this.errorProviderIntegerHigherZero.BlinkRate = 300;
            this.errorProviderIntegerHigherZero.ContainerControl = this;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.textBox13);
            this.tabPage3.Controls.Add(this.textBox12);
            this.tabPage3.Controls.Add(this.textBox11);
            this.tabPage3.Controls.Add(this.textBox10);
            this.tabPage3.Controls.Add(this.textBox3);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(683, 471);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Tabelleneinstellungen";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Location = new System.Drawing.Point(28, 168);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(626, 276);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Beispiel:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Image = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Resources.Tabelleneinstellungen;
            this.pictureBox1.Location = new System.Drawing.Point(3, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(620, 254);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 23;
            this.pictureBox1.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 125);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(239, 17);
            this.label15.TabIndex = 21;
            this.label15.Text = "Spaltennummer der E-Mail Adressen";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 96);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(212, 17);
            this.label14.TabIndex = 19;
            this.label14.Text = "Zeilennummer der Überschriften";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 67);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(207, 17);
            this.label13.TabIndex = 17;
            this.label13.Text = "Erste Spalte mit Detaileinträgen";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 38);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(186, 17);
            this.label12.TabIndex = 15;
            this.label12.Text = "Erste Zeile mit Zeiteinträgen";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(231, 17);
            this.label11.TabIndex = 13;
            this.label11.Text = "Spaltennummer der Familiennamen";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnSelectPicture);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.textBox9);
            this.tabPage2.Controls.Add(this.textBox8);
            this.tabPage2.Controls.Add(this.textBox6);
            this.tabPage2.Controls.Add(this.textBox4);
            this.tabPage2.Controls.Add(this.textBox5);
            this.tabPage2.Controls.Add(this.textBox7);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(683, 471);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "E-Mail Vorlage";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnSelectPicture
            // 
            this.btnSelectPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectPicture.Location = new System.Drawing.Point(577, 437);
            this.btnSelectPicture.Name = "btnSelectPicture";
            this.btnSelectPicture.Size = new System.Drawing.Size(100, 28);
            this.btnSelectPicture.TabIndex = 25;
            this.btnSelectPicture.Text = "Auswählen";
            this.btnSelectPicture.UseVisualStyleBackColor = true;
            this.btnSelectPicture.Click += new System.EventHandler(this.btnSelectPicture_Click);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(24, 443);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 17);
            this.label10.TabIndex = 23;
            this.label10.Text = "E-Mail Bild";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(23, 238);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 17);
            this.label9.TabIndex = 21;
            this.label9.Text = "E-Mail Footer";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 17);
            this.label7.TabIndex = 19;
            this.label7.Text = "E-Mail Text";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 17);
            this.label5.TabIndex = 17;
            this.label5.Text = "Betreff";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "BCC:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 17);
            this.label8.TabIndex = 13;
            this.label8.Text = "Von:";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBox14);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.numVersandBlock);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.checkBox1);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.tbxCryptedPassword);
            this.tabPage1.Controls.Add(this.textBox2);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.tbxSmtpServer);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(683, 471);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "E-Mail Konto";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 94);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 17);
            this.label16.TabIndex = 17;
            this.label16.Text = "Verbindung:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 17);
            this.label4.TabIndex = 15;
            this.label4.Text = "Kennwort:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 17);
            this.label3.TabIndex = 13;
            this.label3.Text = "Benutzername";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(362, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Port:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "Postausgangsserver";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(691, 500);
            this.tabControl1.TabIndex = 10;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 122);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(106, 17);
            this.label17.TabIndex = 19;
            this.label17.Text = "Versandblöcke:";
            // 
            // numVersandBlock
            // 
            this.numVersandBlock.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default, "SmtpSendBlock", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.numVersandBlock.Location = new System.Drawing.Point(156, 120);
            this.numVersandBlock.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numVersandBlock.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numVersandBlock.Name = "numVersandBlock";
            this.numVersandBlock.Size = new System.Drawing.Size(62, 23);
            this.numVersandBlock.TabIndex = 20;
            this.numVersandBlock.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numVersandBlock.Value = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default.SmtpSendBlock;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default.SmtpServerUseSsl;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default, "SmtpServerUseSsl", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBox1.Location = new System.Drawing.Point(156, 93);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(200, 21);
            this.checkBox1.TabIndex = 18;
            this.checkBox1.Text = "Erlaube SSL-Verbindungen";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // tbxCryptedPassword
            // 
            this.tbxCryptedPassword.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default, "SmtpClientPassword", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbxCryptedPassword.Location = new System.Drawing.Point(156, 64);
            this.tbxCryptedPassword.Name = "tbxCryptedPassword";
            this.tbxCryptedPassword.PasswordChar = '*';
            this.tbxCryptedPassword.Size = new System.Drawing.Size(200, 23);
            this.tbxCryptedPassword.TabIndex = 16;
            this.tbxCryptedPassword.Text = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default.SmtpClientPassword;
            // 
            // textBox2
            // 
            this.textBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default, "SmtpClientUsername", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox2.Location = new System.Drawing.Point(156, 35);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(200, 23);
            this.textBox2.TabIndex = 14;
            this.textBox2.Text = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default.SmtpClientUsername;
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default, "SmtpServerPort", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox1.Location = new System.Drawing.Point(406, 6);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(66, 23);
            this.textBox1.TabIndex = 12;
            this.textBox1.Text = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default.SmtpServerPort;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox1.Validating += new System.ComponentModel.CancelEventHandler(this.tbx_ValidatingIntegerHigherZero);
            this.textBox1.Validated += new System.EventHandler(this.tbx_ValidatedIntegerHigherZero);
            // 
            // tbxSmtpServer
            // 
            this.tbxSmtpServer.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default, "SmtpServerHost", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbxSmtpServer.Location = new System.Drawing.Point(156, 6);
            this.tbxSmtpServer.Name = "tbxSmtpServer";
            this.tbxSmtpServer.Size = new System.Drawing.Size(200, 23);
            this.tbxSmtpServer.TabIndex = 10;
            this.tbxSmtpServer.Text = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default.SmtpServerHost;
            // 
            // textBox9
            // 
            this.textBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox9.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default, "SmtpMailPicture", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox9.Location = new System.Drawing.Point(130, 440);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(441, 23);
            this.textBox9.TabIndex = 24;
            this.textBox9.Text = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default.SmtpMailPicture;
            // 
            // textBox8
            // 
            this.textBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox8.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default, "SmtpMailFooter", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox8.Location = new System.Drawing.Point(130, 235);
            this.textBox8.Multiline = true;
            this.textBox8.Name = "textBox8";
            this.textBox8.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox8.Size = new System.Drawing.Size(546, 196);
            this.textBox8.TabIndex = 22;
            this.textBox8.Text = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default.SmtpMailFooter;
            // 
            // textBox6
            // 
            this.textBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox6.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default, "SmtpClientMailHeader", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox6.Location = new System.Drawing.Point(130, 93);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox6.Size = new System.Drawing.Size(547, 136);
            this.textBox6.TabIndex = 20;
            this.textBox6.Text = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default.SmtpClientMailHeader;
            // 
            // textBox4
            // 
            this.textBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox4.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default, "SmtpClientSubject", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox4.Location = new System.Drawing.Point(131, 64);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(545, 23);
            this.textBox4.TabIndex = 18;
            this.textBox4.Text = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default.SmtpClientSubject;
            // 
            // textBox5
            // 
            this.textBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox5.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default, "SmtpClientBcc", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox5.Location = new System.Drawing.Point(131, 35);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(545, 23);
            this.textBox5.TabIndex = 16;
            this.textBox5.Text = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default.SmtpClientBcc;
            // 
            // textBox7
            // 
            this.textBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox7.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default, "SmtpClientFrom", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox7.Location = new System.Drawing.Point(131, 6);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(546, 23);
            this.textBox7.TabIndex = 14;
            this.textBox7.Text = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default.SmtpClientFrom;
            // 
            // textBox13
            // 
            this.textBox13.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default, "SheetMailAddressColumnIndex", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox13.Location = new System.Drawing.Point(296, 122);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(66, 23);
            this.textBox13.TabIndex = 22;
            this.textBox13.Text = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default.SheetMailAddressColumnIndex;
            this.textBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox13.Validating += new System.ComponentModel.CancelEventHandler(this.tbx_ValidatingIntegerHigherZero);
            this.textBox13.Validated += new System.EventHandler(this.tbx_ValidatedIntegerHigherZero);
            // 
            // textBox12
            // 
            this.textBox12.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default, "SheetHeaderDataRowIndex", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox12.Location = new System.Drawing.Point(296, 93);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(66, 23);
            this.textBox12.TabIndex = 20;
            this.textBox12.Text = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default.SheetHeaderDataRowIndex;
            this.textBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox12.Validating += new System.ComponentModel.CancelEventHandler(this.tbx_ValidatingIntegerHigherZero);
            this.textBox12.Validated += new System.EventHandler(this.tbx_ValidatedIntegerHigherZero);
            // 
            // textBox11
            // 
            this.textBox11.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default, "SheetFirstDictionaryColumnIndex", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox11.Location = new System.Drawing.Point(296, 64);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(66, 23);
            this.textBox11.TabIndex = 18;
            this.textBox11.Text = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default.SheetFirstDictionaryColumnIndex;
            this.textBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox11.Validating += new System.ComponentModel.CancelEventHandler(this.tbx_ValidatingIntegerHigherZero);
            this.textBox11.Validated += new System.EventHandler(this.tbx_ValidatedIntegerHigherZero);
            // 
            // textBox10
            // 
            this.textBox10.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default, "SheetFirstDataRowIndex", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox10.Location = new System.Drawing.Point(296, 35);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(66, 23);
            this.textBox10.TabIndex = 16;
            this.textBox10.Text = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default.SheetFirstDataRowIndex;
            this.textBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox10.Validating += new System.ComponentModel.CancelEventHandler(this.tbx_ValidatingIntegerHigherZero);
            this.textBox10.Validated += new System.EventHandler(this.tbx_ValidatedIntegerHigherZero);
            // 
            // textBox3
            // 
            this.textBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default, "SheetFamilyColumnIndex", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox3.Location = new System.Drawing.Point(296, 6);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(66, 23);
            this.textBox3.TabIndex = 14;
            this.textBox3.Text = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Settings.Default.SheetFamilyColumnIndex;
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox3.Validating += new System.ComponentModel.CancelEventHandler(this.tbx_ValidatingIntegerHigherZero);
            this.textBox3.Validated += new System.EventHandler(this.tbx_ValidatedIntegerHigherZero);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(224, 122);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(229, 17);
            this.label18.TabIndex = 21;
            this.label18.Text = "Nachrichten gleichzeitig versenden";
            // 
            // textBox14
            // 
            this.textBox14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.textBox14.Location = new System.Drawing.Point(156, 149);
            this.textBox14.Multiline = true;
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(297, 119);
            this.textBox14.TabIndex = 22;
            this.textBox14.Text = resources.GetString("textBox14.Text");
            // 
            // FrmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 560);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Einstellungen";
            this.Load += new System.EventHandler(this.FrmSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderIntegerHigherZero)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numVersandBlock)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.OpenFileDialog openFileDialogPicture;
        private System.Windows.Forms.ErrorProvider errorProviderIntegerHigherZero;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox tbxCryptedPassword;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox tbxSmtpServer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnSelectPicture;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown numVersandBlock;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label18;
    }
}
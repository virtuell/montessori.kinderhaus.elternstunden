﻿namespace Montessori.Kinderhaus.Elternstunden.App
{
    partial class FrmAbout
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblProdukt = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblCopyright = new System.Windows.Forms.Label();
            this.lblFirma = new System.Windows.Forms.Label();
            this.lbxKomponenten = new System.Windows.Forms.ListBox();
            this.lnkExternalComponents = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Image = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Resources.montessori_kinderhaus_gellershagen;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(584, 134);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lblProdukt
            // 
            this.lblProdukt.AutoSize = true;
            this.lblProdukt.Location = new System.Drawing.Point(13, 152);
            this.lblProdukt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblProdukt.Name = "lblProdukt";
            this.lblProdukt.Size = new System.Drawing.Size(57, 17);
            this.lblProdukt.TabIndex = 0;
            this.lblProdukt.Text = "Produkt";
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnOK.Location = new System.Drawing.Point(468, 366);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 28);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblCopyright
            // 
            this.lblCopyright.AutoSize = true;
            this.lblCopyright.Location = new System.Drawing.Point(13, 202);
            this.lblCopyright.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(68, 17);
            this.lblCopyright.TabIndex = 2;
            this.lblCopyright.Text = "Copyright";
            // 
            // lblFirma
            // 
            this.lblFirma.AutoSize = true;
            this.lblFirma.Location = new System.Drawing.Point(13, 177);
            this.lblFirma.Name = "lblFirma";
            this.lblFirma.Size = new System.Drawing.Size(43, 17);
            this.lblFirma.TabIndex = 1;
            this.lblFirma.Text = "Firma";
            // 
            // lbxKomponenten
            // 
            this.lbxKomponenten.FormattingEnabled = true;
            this.lbxKomponenten.ItemHeight = 16;
            this.lbxKomponenten.Location = new System.Drawing.Point(16, 229);
            this.lbxKomponenten.Name = "lbxKomponenten";
            this.lbxKomponenten.ScrollAlwaysVisible = true;
            this.lbxKomponenten.Size = new System.Drawing.Size(445, 132);
            this.lbxKomponenten.TabIndex = 3;
            // 
            // lnkExternalComponents
            // 
            this.lnkExternalComponents.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lnkExternalComponents.AutoSize = true;
            this.lnkExternalComponents.Location = new System.Drawing.Point(13, 372);
            this.lnkExternalComponents.Name = "lnkExternalComponents";
            this.lnkExternalComponents.Size = new System.Drawing.Size(342, 17);
            this.lnkExternalComponents.TabIndex = 5;
            this.lnkExternalComponents.TabStop = true;
            this.lnkExternalComponents.Text = "Lizenzinformation zu externen Softwarekomponenten";
            this.lnkExternalComponents.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkExternalComponents_LinkClicked);
            // 
            // FrmAbout
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.btnOK;
            this.ClientSize = new System.Drawing.Size(584, 409);
            this.Controls.Add(this.lnkExternalComponents);
            this.Controls.Add(this.lbxKomponenten);
            this.Controls.Add(this.lblFirma);
            this.Controls.Add(this.lblCopyright);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblProdukt);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmAbout";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Info über...";
            this.Load += new System.EventHandler(this.FrmAbout_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblProdukt;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblCopyright;
        private System.Windows.Forms.Label lblFirma;
        private System.Windows.Forms.ListBox lbxKomponenten;
        private System.Windows.Forms.LinkLabel lnkExternalComponents;
    }
}
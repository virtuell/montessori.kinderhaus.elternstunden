﻿namespace Montessori.Kinderhaus.Elternstunden.App
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelpInfoAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelpDoku = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblSelectSheet = new System.Windows.Forms.Label();
            this.comboSelectSheet = new System.Windows.Forms.ComboBox();
            this.openFileDialogSheet = new System.Windows.Forms.OpenFileDialog();
            this.dgvOverview = new System.Windows.Forms.DataGridView();
            this.btnSend = new System.Windows.Forms.Button();
            this.lnkSelectAll = new System.Windows.Forms.LinkLabel();
            this.lnkInverseSelection = new System.Windows.Forms.LinkLabel();
            this.progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.progressText = new System.Windows.Forms.ToolStripStatusLabel();
            this.bgwWorker = new System.ComponentModel.BackgroundWorker();
            this.familyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mailAddressDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reportOverviewDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOverview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileOpen,
            this.menuSettings,
            this.menuExit,
            this.menuHelp});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(634, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuFileOpen
            // 
            this.menuFileOpen.Image = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Resources.Folder_open_32xMD_exp;
            this.menuFileOpen.Name = "menuFileOpen";
            this.menuFileOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.menuFileOpen.Size = new System.Drawing.Size(116, 36);
            this.menuFileOpen.Text = "Datei öffnen";
            this.menuFileOpen.Click += new System.EventHandler(this.menuFileOpen_Click);
            // 
            // menuSettings
            // 
            this.menuSettings.Image = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Resources.Settings_32xMD;
            this.menuSettings.Name = "menuSettings";
            this.menuSettings.Size = new System.Drawing.Size(122, 36);
            this.menuSettings.Text = "Einstellungen";
            this.menuSettings.Click += new System.EventHandler(this.menuSettings_Click);
            // 
            // menuHelp
            // 
            this.menuHelp.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.menuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuHelpInfoAbout,
            this.menuHelpDoku});
            this.menuHelp.Image = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Resources.StatusHelp_grey_32xMD;
            this.menuHelp.Name = "menuHelp";
            this.menuHelp.Size = new System.Drawing.Size(76, 36);
            this.menuHelp.Text = "Hilfe";
            // 
            // menuHelpInfoAbout
            // 
            this.menuHelpInfoAbout.Image = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Resources.StatusInformation_grey_32xMD;
            this.menuHelpInfoAbout.Name = "menuHelpInfoAbout";
            this.menuHelpInfoAbout.Size = new System.Drawing.Size(302, 38);
            this.menuHelpInfoAbout.Text = "Info über Elternstunden";
            this.menuHelpInfoAbout.Click += new System.EventHandler(this.menuHelpInfoAbout_Click);
            // 
            // menuHelpDoku
            // 
            this.menuHelpDoku.Image = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Resources.Web_32x;
            this.menuHelpDoku.Name = "menuHelpDoku";
            this.menuHelpDoku.Size = new System.Drawing.Size(302, 38);
            this.menuHelpDoku.Text = "Elternstunden Webseite/Dokumentation";
            this.menuHelpDoku.Click += new System.EventHandler(this.menuHelpDoku_Click);
            // 
            // menuExit
            // 
            this.menuExit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.menuExit.Image = global::Montessori.Kinderhaus.Elternstunden.App.Properties.Resources.Close_16x;
            this.menuExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.menuExit.Name = "menuExit";
            this.menuExit.Size = new System.Drawing.Size(81, 36);
            this.menuExit.Text = "Beenden";
            this.menuExit.Click += new System.EventHandler(this.menuExit_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.progressBar,
            this.progressText});
            this.statusStrip1.Location = new System.Drawing.Point(0, 333);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(634, 28);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblSelectSheet
            // 
            this.lblSelectSheet.AutoSize = true;
            this.lblSelectSheet.Location = new System.Drawing.Point(12, 59);
            this.lblSelectSheet.Name = "lblSelectSheet";
            this.lblSelectSheet.Size = new System.Drawing.Size(130, 17);
            this.lblSelectSheet.TabIndex = 2;
            this.lblSelectSheet.Text = "Tabelle auswählen:";
            // 
            // comboSelectSheet
            // 
            this.comboSelectSheet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSelectSheet.FormattingEnabled = true;
            this.comboSelectSheet.Location = new System.Drawing.Point(148, 56);
            this.comboSelectSheet.Name = "comboSelectSheet";
            this.comboSelectSheet.Size = new System.Drawing.Size(200, 24);
            this.comboSelectSheet.TabIndex = 3;
            this.comboSelectSheet.SelectedIndexChanged += new System.EventHandler(this.comboSelectSheet_SelectedIndexChanged);
            // 
            // openFileDialogSheet
            // 
            this.openFileDialogSheet.Title = "Datei mit aktuellen Elternstunden auswählen...";
            this.openFileDialogSheet.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialogSheet_FileOk);
            // 
            // dgvOverview
            // 
            this.dgvOverview.AllowUserToAddRows = false;
            this.dgvOverview.AllowUserToDeleteRows = false;
            this.dgvOverview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvOverview.AutoGenerateColumns = false;
            this.dgvOverview.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvOverview.ColumnHeadersHeight = 30;
            this.dgvOverview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.familyDataGridViewTextBoxColumn,
            this.mailAddressDataGridViewTextBoxColumn,
            this.reportOverviewDataGridViewTextBoxColumn});
            this.dgvOverview.DataSource = this.reportBindingSource;
            this.dgvOverview.Location = new System.Drawing.Point(15, 90);
            this.dgvOverview.Name = "dgvOverview";
            this.dgvOverview.ReadOnly = true;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvOverview.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvOverview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvOverview.Size = new System.Drawing.Size(607, 199);
            this.dgvOverview.TabIndex = 4;
            this.dgvOverview.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvOverview_CellFormatting);
            // 
            // btnSend
            // 
            this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSend.Location = new System.Drawing.Point(15, 295);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(333, 35);
            this.btnSend.TabIndex = 5;
            this.btnSend.Text = "E-Mails an ausgewählte Familien senden...";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // lnkSelectAll
            // 
            this.lnkSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lnkSelectAll.AutoSize = true;
            this.lnkSelectAll.Location = new System.Drawing.Point(354, 304);
            this.lnkSelectAll.Name = "lnkSelectAll";
            this.lnkSelectAll.Size = new System.Drawing.Size(109, 17);
            this.lnkSelectAll.TabIndex = 6;
            this.lnkSelectAll.TabStop = true;
            this.lnkSelectAll.Text = "Alles auswählen";
            this.lnkSelectAll.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkSelectAll_LinkClicked);
            // 
            // lnkInverseSelection
            // 
            this.lnkInverseSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lnkInverseSelection.AutoSize = true;
            this.lnkInverseSelection.Location = new System.Drawing.Point(469, 304);
            this.lnkInverseSelection.Name = "lnkInverseSelection";
            this.lnkInverseSelection.Size = new System.Drawing.Size(127, 17);
            this.lnkInverseSelection.TabIndex = 7;
            this.lnkInverseSelection.TabStop = true;
            this.lnkInverseSelection.Text = "Auswahl umkehren";
            this.lnkInverseSelection.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkInverseSelection_LinkClicked);
            // 
            // progressBar
            // 
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(100, 22);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            // 
            // progressText
            // 
            this.progressText.Name = "progressText";
            this.progressText.Size = new System.Drawing.Size(73, 23);
            this.progressText.Text = "progressText";
            // 
            // bgwWorker
            // 
            this.bgwWorker.WorkerReportsProgress = true;
            this.bgwWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwWorker_DoWork);
            this.bgwWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwWorker_ProgressChanged);
            this.bgwWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwWorker_RunWorkerCompleted);
            // 
            // familyDataGridViewTextBoxColumn
            // 
            this.familyDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.familyDataGridViewTextBoxColumn.DataPropertyName = "Family";
            this.familyDataGridViewTextBoxColumn.HeaderText = "Familie";
            this.familyDataGridViewTextBoxColumn.Name = "familyDataGridViewTextBoxColumn";
            this.familyDataGridViewTextBoxColumn.ReadOnly = true;
            this.familyDataGridViewTextBoxColumn.Width = 77;
            // 
            // mailAddressDataGridViewTextBoxColumn
            // 
            this.mailAddressDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.mailAddressDataGridViewTextBoxColumn.DataPropertyName = "MailAddress";
            this.mailAddressDataGridViewTextBoxColumn.HeaderText = "E-Mail";
            this.mailAddressDataGridViewTextBoxColumn.Name = "mailAddressDataGridViewTextBoxColumn";
            this.mailAddressDataGridViewTextBoxColumn.ReadOnly = true;
            this.mailAddressDataGridViewTextBoxColumn.Width = 72;
            // 
            // reportOverviewDataGridViewTextBoxColumn
            // 
            this.reportOverviewDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.reportOverviewDataGridViewTextBoxColumn.DataPropertyName = "ReportOverview";
            this.reportOverviewDataGridViewTextBoxColumn.HeaderText = "Überblick";
            this.reportOverviewDataGridViewTextBoxColumn.Name = "reportOverviewDataGridViewTextBoxColumn";
            this.reportOverviewDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reportBindingSource
            // 
            this.reportBindingSource.DataSource = typeof(Montessori.Kinderhaus.Elternstunden.Business.Models.Report);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 361);
            this.Controls.Add(this.lnkInverseSelection);
            this.Controls.Add(this.lnkSelectAll);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.dgvOverview);
            this.Controls.Add(this.comboSelectSheet);
            this.Controls.Add(this.lblSelectSheet);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(650, 400);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Elternstunden E-Mails";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOverview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuFileOpen;
        private System.Windows.Forms.ToolStripMenuItem menuSettings;
        private System.Windows.Forms.ToolStripMenuItem menuHelp;
        private System.Windows.Forms.ToolStripMenuItem menuHelpInfoAbout;
        private System.Windows.Forms.ToolStripMenuItem menuExit;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Label lblSelectSheet;
        private System.Windows.Forms.ComboBox comboSelectSheet;
        private System.Windows.Forms.OpenFileDialog openFileDialogSheet;
        private System.Windows.Forms.DataGridView dgvOverview;
        private System.Windows.Forms.BindingSource reportBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn familyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mailAddressDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reportOverviewDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.LinkLabel lnkSelectAll;
        private System.Windows.Forms.LinkLabel lnkInverseSelection;
        private System.Windows.Forms.ToolStripMenuItem menuHelpDoku;
        private System.Windows.Forms.ToolStripProgressBar progressBar;
        private System.Windows.Forms.ToolStripStatusLabel progressText;
        private System.ComponentModel.BackgroundWorker bgwWorker;
    }
}


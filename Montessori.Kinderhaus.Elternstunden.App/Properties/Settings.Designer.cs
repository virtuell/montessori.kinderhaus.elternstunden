﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Montessori.Kinderhaus.Elternstunden.App.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.8.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("localhost")]
        public string SmtpServerHost {
            get {
                return ((string)(this["SmtpServerHost"]));
            }
            set {
                this["SmtpServerHost"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Aktueller Stand Elternstunden")]
        public string SmtpClientSubject {
            get {
                return ((string)(this["SmtpClientSubject"]));
            }
            set {
                this["SmtpClientSubject"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SmtpMailPicture {
            get {
                return ((string)(this["SmtpMailPicture"]));
            }
            set {
                this["SmtpMailPicture"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SmtpClientPassword {
            get {
                return ((string)(this["SmtpClientPassword"]));
            }
            set {
                this["SmtpClientPassword"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Hallo liebe Eltern,\r\n\r\nmit dieser E-Mail erhaltet ihr den aktuellen Stand über eu" +
            "re geleisteten Elternstunden:\r\n\r\n\r\n        ")]
        public string SmtpClientMailHeader {
            get {
                return ((string)(this["SmtpClientMailHeader"]));
            }
            set {
                this["SmtpClientMailHeader"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("gellershagen@montessori-bielefeld.de")]
        public string SmtpClientUsername {
            get {
                return ((string)(this["SmtpClientUsername"]));
            }
            set {
                this["SmtpClientUsername"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("gellershagen@montessori-bielefeld.de")]
        public string SmtpClientFrom {
            get {
                return ((string)(this["SmtpClientFrom"]));
            }
            set {
                this["SmtpClientFrom"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("587")]
        public string SmtpServerPort {
            get {
                return ((string)(this["SmtpServerPort"]));
            }
            set {
                this["SmtpServerPort"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SmtpClientBcc {
            get {
                return ((string)(this["SmtpClientBcc"]));
            }
            set {
                this["SmtpClientBcc"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("\r\n\r\n\r\nViele Grüße,\r\n\r\nVera\r\n_____________________________________\r\n\r\n###BILD###\r\n" +
            "\r\nwww.montessori-bielefeld.de\r\n\r\n\r\nKopernikusstraße 70\r\n33613 Bielefeld\r\n\r\nTel: " +
            "0521/329 20 200\r\nFax:0521/329 20 205\r\n        ")]
        public string SmtpMailFooter {
            get {
                return ((string)(this["SmtpMailFooter"]));
            }
            set {
                this["SmtpMailFooter"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1")]
        public string SheetFamilyColumnIndex {
            get {
                return ((string)(this["SheetFamilyColumnIndex"]));
            }
            set {
                this["SheetFamilyColumnIndex"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("3")]
        public string SheetFirstDataRowIndex {
            get {
                return ((string)(this["SheetFirstDataRowIndex"]));
            }
            set {
                this["SheetFirstDataRowIndex"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("3")]
        public string SheetFirstDictionaryColumnIndex {
            get {
                return ((string)(this["SheetFirstDictionaryColumnIndex"]));
            }
            set {
                this["SheetFirstDictionaryColumnIndex"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1")]
        public string SheetHeaderDataRowIndex {
            get {
                return ((string)(this["SheetHeaderDataRowIndex"]));
            }
            set {
                this["SheetHeaderDataRowIndex"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("2")]
        public string SheetMailAddressColumnIndex {
            get {
                return ((string)(this["SheetMailAddressColumnIndex"]));
            }
            set {
                this["SheetMailAddressColumnIndex"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("650, 400")]
        public global::System.Drawing.Size Form1Size {
            get {
                return ((global::System.Drawing.Size)(this["Form1Size"]));
            }
            set {
                this["Form1Size"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Normal")]
        public global::System.Windows.Forms.FormWindowState Form1State {
            get {
                return ((global::System.Windows.Forms.FormWindowState)(this["Form1State"]));
            }
            set {
                this["Form1State"] = value;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Elternstunden E-Mails")]
        public string Form1Text {
            get {
                return ((string)(this["Form1Text"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("https://bitbucket.org/virtuell/montessori.kinderhaus.elternstunden/wiki/Home")]
        public string WikiUrlRepository {
            get {
                return ((string)(this["WikiUrlRepository"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("https://bitbucket.org/virtuell/montessori.kinderhaus.elternstunden/wiki/Externe-K" +
            "omponenten")]
        public string WikiUrlExternalComponents {
            get {
                return ((string)(this["WikiUrlExternalComponents"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("VXu2GF8m")]
        public string CryptoSecretKey {
            get {
                return ((string)(this["CryptoSecretKey"]));
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool SmtpServerUseSsl {
            get {
                return ((bool)(this["SmtpServerUseSsl"]));
            }
            set {
                this["SmtpServerUseSsl"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string CurrentVersion {
            get {
                return ((string)(this["CurrentVersion"]));
            }
            set {
                this["CurrentVersion"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool UpdatedUserSettings {
            get {
                return ((bool)(this["UpdatedUserSettings"]));
            }
            set {
                this["UpdatedUserSettings"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("10")]
        public decimal SmtpSendBlock {
            get {
                return ((decimal)(this["SmtpSendBlock"]));
            }
            set {
                this["SmtpSendBlock"] = value;
            }
        }
    }
}
